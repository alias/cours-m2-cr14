# Static Analysis and Optimizing Compilers (CR14)

_academic year 2022-23_

Course given at [ENS-Lyon](http://www.ens-lyon.fr/DI/) by
[Christophe Alias (CR Inria)](http://perso.ens-lyon.fr/christophe.alias/), 
[Laure Gonnord (Prof. Grenoble INP)](https://laure.gonnord.org/pro), and
[Yannick Zakowski (CR Inria)](http://perso.ens-lyon.fr/yannick.zakowski/) (person in charge). We all belong to the [CASH Team](http://www.ens-lyon.fr/LIP/CASH/) of the LIP.

# Planning

## Part 1: Optimizing Compilers with the Polyhedral Model (C. Alias)

* [Handouts](2022/part-1-polyhedral-model/all-handout.pdf)

* [Lecture 1: Introduction to optimizing compilers](2022/part-1-polyhedral-model/cours-1-intro.pdf) [(handout)](2022/part-1-polyhedral-model/cours-1-intro-handout.pdf)

* [Lecture 2: Data Transfers: Architectures, Models and Optimizations](2022/part-1-polyhedral-model/cours-2-io.pdf) [(handout)](2022/part-1-polyhedral-model/cours-2-io-handout.pdf)

* [Lecture 3: Polyhedral Model](2022/part-1-polyhedral-model/cours-3-poly.pdf) [(handout)](2022/part-1-polyhedral-model/cours-3-poly-handout.pdf)

* [Lecture 4: Linear Scheduling](2022/part-1-polyhedral-model/cours-4-linear-scheduling.pdf) [(handout)](2022/part-1-polyhedral-model/cours-4-linear-scheduling-handout.pdf)

* [Lecture 5: Linear Loop Tiling](2022/part-1-polyhedral-model/cours-5-linear-tiling.pdf) [(handout)](2022/part-1-polyhedral-model/cours-5-linear-tiling-handout.pdf)

* [Lecture 6: General Loop Nests](2022/part-1-polyhedral-model/cours-6-non-perfect.pdf) [(handout)](2022/part-1-polyhedral-model/cours-6-non-perfect-handout.pdf)

* [Homework](2022/part-1-polyhedral-model/homework/hw-part1.pdf). Resources: [code-template.cc](2022/part-1-polyhedral-model/homework/code-template.cc), [iscc](2022/part-1-polyhedral-model/tp1/iscc.tgz), [iscc documentation](2022/part-1-polyhedral-model/homework/iscc-manual.pdf) (see pages 6-21), [iscc tutorial](2022/part-1-polyhedral-model/homework/iscc-tutorial.pdf). Send to christophe.alias(arobase)ens-lyon.fr by November 1.

### Related M2 internships + PhD proposals

* [Program equivalence (M2)](2022/part-1-polyhedral-model/stages/Stage-DFG-NN-2022-2023-Touati-Formenti-Alias-ENS.pdf)

* [Parallelizing sparse computations (M2+PhD)](2022/part-1-polyhedral-model/stages/stage-sparse.pdf)

* [Compilation-guided runtime scheduling (M2)](2022/part-1-polyhedral-model/stages/scheduling-M2.pdf)


## Part 2: Static Analysis with Abstract Interpretation (L. Gonnord)

## Part 3: Certified Analysis, Certified Compilation (Y. Zakowski)

* Lecture 1, October 12: Introduction to verified compilation, elements of mechanized semantics in Coq ([slides](2022/part3-verified-compil/intro.pdf), blackboard and Coq)
    - [Introduction file](2022/part3-verified-compil/intro_coq.v) and [Browsable version](https://perso.ens-lyon.fr/yannick.zakowski/CR14/intro_coq.html)
  - Homework: get a working installation of Coq, step through the file seen in class, and write down any aspect you do not understand to ask for explanation either by mail or in class.
* Lecture 2, October 18: Writing a compiler for Imp, expressing and proving its correctness for terminating commands (blackboard and Coq)
  - [Coq material](2022/part3-verified-compil/Compil.zip)
  - Homework due for Tuesday 25. Implement the compilation of boolean expressions [compile_bexp] following the scheme seen in class.
   	Observe the remark associated to the example on line 249 (a TODO marker can be found there): modify the function [compile_com] to resolve this source of inefficiency.
    Consequently fix the admits in the [Compil.v] file, and have the project compiling again.
* Lecture 3, October 19: Correctness of diverging commands through simulation diagrams (blackboard and Coq)
  - [Updated Coq material](2022/part3-verified-compil/Compil2.zip)
* Lecture 4, October 25: Dataflow analysis, fixed points in Coq and dead code elimination
* Lecture 5, October 26: Abstract interpretation
  - [Coq material, file 1](2022/part3-verified-compil/AbstrInterp.v) [ and file 2](2022/part3-verified-compil/AbstrInterp2.v)

Homework, due for Sunday, November 13th.
  - In file [AbstrInterp.v](2022/part3-verified-compil/AbstrInterp.v), implement and prove correct the domain of signs for the simple interface. To do so, fill in the module `Sign` at the bottom of the file, enforcing it to satisfy the `VALUE_ABSTRACTION` interface. Once defined, test your domain on a couple of examples.
  - In file [AbstrInterp2.v](2022/part3-verified-compil/AbstrInterp2.v), implement and prove correct the domain of intervals for the advanced interface. To do so, fill in the module `Intervals` at the bottom of the file, enforcing it to satisfy the `VALUE_ABSTRACTION` interface.
  - Note1: If you are stuck on some proofs by lack of familiarity with the Coq Proof Assistant, you may admit the proofs (using "Admitted") and instead write an informal proof in comments.
  - Note2: the second exercise is quite long. It is absolutely fine to only cover part of it if you are having a hard time to tackle it.

# Evaluation

## Practical instructions

Please be sure to note that the evaluation will take place on Tuesday November the 8th, at 3:45pm to 6:30pm.

<b>Please send your slides by mail to Yannick at least one hour before the session</b>.

The final evaluation will consist in an oral and written presentation of a research paper.

## Important notes:

* it is absolutely normal if you do not understand all details of the paper! Your task is to understand as much of it as possible and report on it, but it is absolutely fine to say that you failed to understand a technical section.
* In the context of this exercise, you are encouraged to directly use the content from the paper: examples, figures and so on can be reused as is for your explanation, both in the oral and written presentation.

## Preliminary: choice of paper

Please choose by Wednesday, 26th October at midnight, at least three papers out of the following 15 that you would like to pick. Indicate your name under these three papers on the following shared document:

https://pads.aliens-lyon.fr/p/zlEOlXcffMzgXxmdatcu

EDIT: Coupling is done, you can find your name under one of the papers below. It is still possible to swap with another student if you both agree, if so please send me an email.

## Instructions -- oral presentation

The presentation will take place on Tuesday, November the 8th, from 3:45pm to 6:30pm.

Each student will present the paper they have chosen according to the following constraints:

* Your presentation should be 10 minutes long, followed by ~3 minutes of questions
* We expect from your presentation:

  - A technical summary of the content of the paper: context, contribution;
  - That you try to put the contribution in the paper in perspective w.r.t. to the course: how does it relate to what you have studied during this course -- or other courses?
  - One slide should discuss the limitations of the paper, both formal (how could the paper have been improved?) and technical (what are the limits of the approach, the future works?)

* You should use slides as supports, but we will not judge the quality of these slides. As long as they are clear and support your presentation cleanly, they will do fine -- do not spend time on polishing them.

## Instructions -- written report

The written report should be handed in before Sunday, November 13th, at midnight.
It should be three pages long, and roughly split into:

* two pages of technical description of the paper;
* one page of a review of the paper: put yourself in the shoes of a reviewer and describe the weakness and strength of the paper according to you.

## List of available papers.

### Part 1:

- [On the Equivalence of Two Systems of Affine Recurrence Equations](2022/part-1-polyhedral-model/eval/barthou03equivalence.pdf)
  -> Jolyne Gatt

- [End-to-End Translation Validation for the Halide Language](2022/part-1-polyhedral-model/eval/clement22validation.pdf)

- [An Optimal Microarchitecture for Stencil Computation Acceleration Based on Non-Uniform Partitioning of Data Reuse Buffers](2022/part-1-polyhedral-model/eval/cong14optimal.pdf)

- [Compiling Affine Loop Nests for a Dynamic Scheduling Runtime on Shared and Distributed Memory](2022/part-1-polyhedral-model/eval/dathathri16compiling.pdf)

- [Polyhedral Expression Propagation](2022/part-1-polyhedral-model/eval/doerfert18prop.pdf)

- [Rec2Poly: Converting Recursions to Polyhedral Optimized Loops Using an Inspector-Executor Strategy](2022/part-1-polyhedral-model/eval/kobeissi20samos.pdf)
  -> Werner Mérian

- [Polyhedral Binary Decision Diagrams for Representing Non-Convex Polyhedra](2022/part-1-polyhedral-model/eval/kruse22bdd.pdf)

- [Prediction and Trace Compression of Data Access Addresses through Nested Loop Recognition](2022/part-1-polyhedral-model/eval/nlr08cgo.pdf)
  -> Louis Gaillard

### Part 2:

* [SecWasm: Information Flow Control for WebAssembly](2022/part2-df-ia/eval/sec_asm_sas22.pdf)
  -> Dominik Stolz

* [Abstract Interpretation Repair](2022/part2-df-ia/eval/abstractinterp_repair.pdf)

* [Static analysis of ReLU neural networks with tropical polyhedra](2022/part2-df-ia/eval/ReLu.pdf)

* [Recovering high-level conditions from binary programs](2022/part2-df-ia/eval/binsec.pdf)

* [Divergence Analysis](2022/part2-df-ia/eval/divergence.pdf)

* [A Decision Tree Abstract Domain for Proving Conditional Termination](2022/part2-df-ia/eval/decision_tree.pdf)

### Part 3:

* [Verified Compilation of CakeML to Multiple Machine-Code Targets](2022/part3-verified-compil/papers/cakeML_low.pdf)
  -> Galaad Langlois

* [Functional Big-Step Semantics](2022/part3-verified-compil/papers/functional_big_step.pdf)
  -> Pierre Goutagny

* [Skeletal Semantics and Their Interpretations](2022/part3-verified-compil/papers/Skeletals.pdf)
  -> Nicolas Nardino

* [Verified Code Generation for the Polyhedral Model](2022/part3-verified-compil/papers/polyhedral_formal.pdf)
  -> Malo Jaffré

* [Simple Relational Correctness Proofs for Static Analyses and Program Transformations](2022/part3-verified-compil/papers/relational_logic.pdf)
  -> Robin Jourde
