# Static Analysis and Optimizing Compilers (CR14)

_academic year 2021-22_

Course given at [ENS-Lyon](http://www.ens-lyon.fr/DI/) by
[Christophe Alias (CR Inria)](http://perso.ens-lyon.fr/christophe.alias/), 
[Laure Gonnord (Prof. Grenoble INP)](https://laure.gonnord.org/pro), and
[Yannick Zakowski (CR Inria)](http://perso.ens-lyon.fr/yannick.zakowski/) (person in charge). We all belong to the [CASH Team](http://www.ens-lyon.fr/LIP/CASH/) of the LIP.

# Planning

## Part 1: Optimizing Compilers with the Polyhedral Model (C. Alias)

* [Lecture 1: Introduction to optimizing compilers](part-1-polyhedral-model/cours-1-intro.pdf) [(handout)](part-1-polyhedral-model/cours-1-intro-handout.pdf)

* [Lecture 2: Data Transfers: Architectures, Models and Optimizations](part-1-polyhedral-model/cours-2-io.pdf) [(handout)](part-1-polyhedral-model/cours-2-io-handout.pdf)

* [Lecture 3: Polyhedral Model](part-1-polyhedral-model/cours-3-poly.pdf) [(handout)](part-1-polyhedral-model/cours-3-poly-handout.pdf)

* [Lecture 4: Linear Scheduling](part-1-polyhedral-model/cours-4-linear-scheduling.pdf) [(handout)](part-1-polyhedral-model/cours-4-linear-scheduling-handout.pdf)

* [Lecture 5: Linear Loop Tiling](part-1-polyhedral-model/cours-5-linear-tiling.pdf) [(handout)](part-1-polyhedral-model/cours-5-linear-tiling-handout.pdf)

* [Lecture 6: General Loop Nests](part-1-polyhedral-model/cours-6-non-perfect.pdf) [(handout)](part-1-polyhedral-model/cours-6-non-perfect-handout.pdf)

* [Homework](part-1-polyhedral-model/homework/hw-part1.pdf). Resources: [code-template.cc](part-1-polyhedral-model/homework/code-template.cc), [iscc](part-1-polyhedral-model/tp1/iscc.tgz), [iscc documentation](part-1-polyhedral-model/homework/iscc-manual.pdf) (see pages 6-21), [iscc tutorial](part-1-polyhedral-model/homework/iscc-tutorial.pdf). Send to christophe.alias(arobase)ens-lyon.fr by November 7.

## Part 2: Static Analysis with Abstract Interpretation (L. Gonnord)

* :book: [Lecture 1 Wed, Sept 22: Mini Intro + Dataflow analyses](part2-df-ia/cr14_dataflow.pdf)

* :pencil: for Wednesday, Sept 29 : tiny homework (slide 77 of previous sequence of slides).

* Additional ressources :movie_camera: [F. Peirera's YT channel - Static Analyses] https://www.youtube.com/playlist?list=PLC-dUCVQghfdu7AG5f_p4oRyKgjDuoAWU)

* :book: [Lectures 2+3 Wed, Sept 29 and Thu, Sept 30: (standard numerical) Abstract Interpretation](part2-df-ia/cr14_ia.pdf)

* :pencil: for Wednesday, Oct 06  : tiny homework (last slide of the standard AI slides).

* :book: [Lecture 4 on Wed, Oct 06: (sparse, numerical) Abstract Interpretation](part2-df-ia/cr14_ia_sparse.pdf)

* :book: [Lecture 5 on Wed, Oct 13th, Pointer analyses](part2-df-ia/cr14_pointers.pdf)

## Part 3: Certified Analysis, Certified Compilation (Y. Zakowski)

* Lecture 1, October 7: Introduction to verified compilation, elements of mechanized semantics in Coq ([slides](intro.pdf), blackboard and Coq)
  - [Introduction file](part3-verified-compil/intro_coq.v) and [Browsable version](https://perso.ens-lyon.fr/yannick.zakowski/CR14/intro_coq.html)
  - Homework: get a working installation of Coq, step through the file seen in class, and write down any aspect you do not understand to ask explanation either by mail or in class.
* Lecture 2, October 14: Writing a compiler for Imp, expressing and proving its correctness for terminating commands (blackboard and Coq)
  - [Coq material](part3-verified-compil/Compil.zip)
  - Homework due for Wednesday 10/20. Implement the compilation of boolean expressions [compile_bexp] following the scheme seen in class.
   	Observe the remark associated to the example on line 249 (a TODO marker can be found there): modify the function [compile_com] to resolve this source of inefficiency
* Lecture 3, October 20: Correctness of diverging commands through simulation diagrams (blackboard and Coq)
  - [Updated Coq material](part3-verified-compil/Compil2.zip)

* Lecture 4, October 21: (Mechanized) Abstract Interpretation
  - [Coq material part 1](part3-verified-compil/AbstrInterp.v) [ and part 2](part3-verified-compil/AbstrInterp2.v)

* Lecture 5, October 27: (Mechanized) Abstract Interpretation (cont.)

  Homework, due for ~~Friday, November 12th~~ **Sunday, November 14th**:

  - In file [AbstrInterp.v](part3-verified-compil/AbstrInterp.v), implement and prove correct the domain of signs for the simple interface. To do so, fill in the module `Sign` at the bottom of the file, enforcing it to satisfy the `VALUE_ABSTRACTION` interface. Once defined, test your domain on a couple of examples.
  - In file [AbstrInterp2.v](part3-verified-compil/AbstrInterp2.v), implement and prove correct the domain of intervals for the advanced interface. To do so, fill in the module `Intervals` at the bottom of the file, enforcing it to satisfy the `VALUE_ABSTRACTION` interface.
  - Note1: If you are stuck on some proofs by lack of familiarity with the Coq Proof Assistant, you may admit the proofs (using "Admitted") and instead write an informal proof in comments.
  - Note2: the second exercise is quite long. It is absolutely fine to only cover part of it if you are having a hard time to tackle it.

Note: this third part of this course is vastly inspired from Xavier Leroy's class at Collège de France. In particular, the Coq file used to illustrate the course are adaptations of Xavier Leroy's material.

# Evaluation

## <b>LATEST PRACTICAL INSTRUCTIONS</b>

Please be sure to note that the evaluation will take place on Wednesday the 10th from 3pm to 6pm. <b>It will not take place in the usual room</b> but in "salle du conseil" i.e. 394 Nord, third floor next to amphi B.
Please try to come slightly early given the packed session we will have.

We will proceed in order of the papers given below, i.e.: Paul Géneau De Lamarlière, Émilie Vidal, Cebere Ioan-Tudor, Teguia Wakam Brice Dufy, Théo Boury, Aliénor Goubault-Larrecq, Alexis Plaquet, Hichma Kari, Brian Bantsoukissa, Lev Pikman, David Julien and Adrien Le Berr.

Finally, in order to be absolutely certain that the show can go on, <b>please send your slides by mail to either Yannick or Laure before the session</b>.

The final evaluation will consist in an oral and written presentation of a research paper.

## Important notes:

* it is absolutely normal if you do not understand all details of the paper! Your task is to understand as much of it as possible and report on it, but it is absolutely fine to say that you failed to understand a technical section.
* In the context of this exercise, you are encouraged to directly use the content from the paper: examples, figures and so on can be reused as is for your explanation, both in the oral and written presentation.

## Preliminary: choice of paper

Please choose by Friday, 29th October at midnight, at least three papers out of the following 15 that you would like to pick. Indicate your name under these three papers on the following shared document:

https://docs.google.com/document/d/16hIFba8oV9t6jV1VAl3y-EW_5gzQfRnusAoOv-NF_rE/edit?usp=sharing


## Instructions -- oral presentation

The presentation will take place on Wednesday, November 10th, from 3pm to 6pm.

Each student will present the paper they have chosen according to the following constraints:

* Your presentation should be 10 minutes long, followed by ~3 minutes of questions
* We expect from your presentation:

  - A technical summary of the content of the paper: context, contribution;
  - That you put the contribution in the paper in perspective w.r.t. to the course: how does it relate to what you have studied during this course -- or other courses?
  - One slide should discuss the limitations of the paper, both formal (how could the paper have been improved?) and technical (what are the limits of the approach, the future works?)

* You should use slides as supports, but we will not judge the quality of these slides. As long as they are clear and support your presentation cleanly, they will do fine -- do not spend too much time polishing them.

## Instructions -- written report

The written report should be handed in before Sunday, November 14th, at midnight.
It should be three pages long, and roughly split into:

* two pages of technical description of the paper;
* one page of a review of the paper: put yourself in the shoes of a reviewer and describe the weakness and strength of the paper according to you.

## List of available papers: allocated. You may still switch for a non-allocated paper if you wish so by sending a mail, or swap with someone by sending me a conjoin mail.

### Part 1:

* [Diamond Tiling](part-1-polyhedral-model/papers/bondhugula2016diamond.pdf)
* [Hybrid Hexagonal/Classical Tiling for GPUs](part-1-polyhedral-model/papers/grosser14hybrid.pdf)
  - Paul Géneau De Lamarlière
* [Parameterized Tiled Loops for Free](part-1-polyhedral-model/papers/lakshmi07pldi.pdf)
  - Vidal Émilie
* [Polyhedral-Based Data Reuse Optimization for Configurable Computing](part-1-polyhedral-model/papers/pouchet13reusefpga.pdf)
  - Cebere Ioan-Tudor
* [pn: a Tool for Improved Derivation of Process Networks](part-1-polyhedral-model/papers/verdoolaege2007pn.pdf)

### Part 2:

* [Divergence Analysis](part2-df-ia/papers/divergence.pdf)
  - Teguia Wakam Brice Dufy
* [Recovering high-level conditions from binary programs](part2-df-ia/papers/binsec.pdf)
  - Théo Boury
* [A Decision Tree Abstract Domain for Proving Conditional Termination](part2-df-ia/papers/decision_tree.pdf)
  - Aliénor Goubault-Larrecq
* [Static analysis of ReLU neural networks with tropical polyhedra](part2-df-ia/papers/ReLu.pdf)
* [How to Compute Worst-Case Execution Time by Optimization Modulo Theory and a Clever Encoding of Program Semantics](part2-df-ia/papers/wcet.pdf)
  - Alexis Plaquet

### Part 3:

* [Formal Verification of Translation Validators](part3-verified-compil/papers/validation-scheduling.pdf)
  - Hichma Kari
* [Formal certification of a compiler back-end, or: programming a compiler with a proof assistant](part3-verified-compil/papers/compcert.pdf)
  - Brian Bantsoukissa
* [Pretty-Big-Step Semantics](part3-verified-compil/papers/pretty.pdf)
  - Lev Pikman
* [A Minimalistic Verified Bootstrapped Compiler](part3-verified-compil/papers/bootstrapped.pdf)
  - David Julien
* [A Formally-Verified C Static Analyzer](part3-verified-compil/papers/verasco.pdf)
  - Adrien Le Berr
