

## Partie 2 : petites preuves sur papier (Laure)

| qui                 | dataflow | Master Theorem | abstract domain |
| GOUTAGNY Pierre     | papier   | papier         | papier          |
| JAFFRÉ Malo         | papier   | papier         | papier          |
| GAILLARD Louis      | papier   | papier         | papier          |
| STOLTZ Dominik      | papier   | papier         | papier          |
| GALAAD Langlois     | papier   | papier         |                 |
| GACK Yolyne (orth?) |          | papier         | papier          |
| JOURDE Robin        | pdf      | papier         | papier          |
| NARDINO Nicolas     | pdf      | pdf            |                 |
| WERNER  Mérian      | pdf      | pdf            |                 |
|                     |          |                |                 |


| LAMIROY Mathis ?  |
|                   |
