(*|
===================================================================
  A gentle overview to the Coq proof assistant
===================================================================

.. coq:: no-in
|*)

Set Implicit Arguments.
Set Strict Implicit.

(*|
.. coq:: no-in
|*)

Module PlayGround.

(*|
Coq: a functional programming language
=======================================

Gallina
--------

Coq relies first and foremost on Gallina, a purely functional language equipped
with a particularly rich type system.
To a very rough first approximation, you can think about the functional fragment of OCaml.

Inductive types
-----------------------

Gallina relies on a powerful tool to define data-types: so-called inductive types.
Intuitively, an inductive type is defined by a finite set of constructors describing
how values of this type can be built.
They encompass the type constructors you may be familiar with, and more.

Sum types
-----------------------

Here are a simple enumerated types containing exactly four values,
as well as the booleans as implemented in the standard library.
|*)

Inductive Suit : Type := Spades | Hearts | Diamonds | Clubs.
Inductive bool : Type := true | false.

(*|
We can pattern match on values of inductive types to start programming
|*)

Definition is_red (card : Suit) : bool :=
  match card with
  | Hearts | Diamonds => true
  | _                 => false
  end.

Definition is_pointy (card : Suit) : bool :=
  match card with
  | Spades | Diamonds => true
  | _                 => false
  end.

(*|
Patterns must always be exhaustive: here we use "_"  as a wild card.

We can ask Coq to compute for us.
|*)

Compute (is_red Hearts).

(*|
But we can also state our first "theorem": writing as an assertion
what we expect to get as an answer.

This time we are provided with an interactive goal: a separate window displays our proof context and goal.
In order to _prove_ this fact, we are going to use some mysterious commands that are not part of Gallina: tactics.

This proof is an example of a proof by simple reduction and reflexivity.
|*)

Example is_red_Hearts:
  is_red Hearts = true.
Proof.
  simpl.       (* We reduce our goal by computation *)
  reflexivity. (* Equality between a term and itself *)
Qed.

(*|
The constructors of an inductive types provide the _only_ ways to construct elements of this type.
We can exploit this fact to prove lemmas by case analysis.
|*)

Lemma pointy_and_red:
  forall (c : Suit),
    is_red c = true ->
    is_pointy c = true ->
    c = Diamonds.
Proof.
  intros c ISRED ISPOINTY. (* We introduce our hypotheses *)
  destruct c. (* Case analysis on c *)
  - simpl in *.
    discriminate ISRED.
  - simpl in *.
    discriminate ISPOINTY.
  - reflexivity.
  - simpl in *.
    discriminate ISPOINTY.
Qed.

(*|
We have been using tactics to prove our lemmas interactively. This may look a little bit mysterious.
Tactics are part of a completely different language from Gallina, called [Ltac]. This language provide
some primitive tactics, and a wild effectful, untyped, language to build new tactics!
We won't discuss much how to write tactics in this introduction, but it allows for some extremely powerful
proof automation.

It however begs a question: isn't this complex unsafe language ruining all our trust?
Luckily, no. Tactics serve only one purpose: they build Gallina terms behind the scene. What guarantees the
validity of the theorem is the [Qed] steps: it uses the trusted core of Coq to type check the term, without
caring by which magical process it may have been built.

Actually, we can even check that I'm not lying: we can print the term that has been built. Discriminate leads to something a bit mysterious, but we can notice the pattern match corresponding to destruct, and the intros becoming binders.
|*)

Print pointy_and_red.

(*|
We will come back to proofs and tactics in greater details in a moment, let's focus on programming for now.

Polymorphism
-----------------------
Gallina supports full polymorphic types.
|*)

Inductive option (A : Type) : Type :=
| None
| Some : A -> option A.

(*|
Product types
-----------------------
Your usual polymorphic product type is just a particular inductive type.
We introduce two
|*)

Inductive prod (A B : Type) : Type :=
| pair : A -> B -> prod A B.

(*|
Recursive types
-----------------------
So far, we have only manipulated base constructors.
Recursive types are simply created by having recursive constructors.
|*)

Inductive nat : Type :=
| O
| S : nat -> nat.

(*|
Now that we have a recursive type, we naturally want to write recursive functions!
|*)

Fixpoint sum (n m : nat) : nat :=
  match n with
  | O => m
  | S n' => S (sum n' m)
  end.
Infix "+" := sum.

(*|
When it comes to proving, implementation matters even more than in usual programming!
The proofs of the following two statements will be radically different:
1. forall n, 0 + n = n
2. forall n, n + 0 = n
Can you foresee why?
|*)

Lemma sum_n_O : forall n,
    O + n = n.
Proof.
  intros n.
  simpl.
  reflexivity.
Qed.

Lemma sum_O_n : forall n,
    n + O = n.
Proof.
  intros n.
  simpl. (* Nothing happens! *)
  Fail reflexivity. (* We cannot conclude by reflexivity *)
  (* We could try by case analysis *)
  destruct n as [| foo].
  reflexivity.
  simpl.
  f_equal. (* We eliminate the S *)
  (* We are back to square one *)
  Undo 6.
  (* We are going to proceed by induction *)
  induction n as [| foo IH].
  - reflexivity. (* Base case *)
  - simpl.
    f_equal.
    apply IH.
Qed.

(*|
Let's play with lists and folds.
|*)

Inductive list (A : Type) : Type :=
| Nil : list A
| Cons : A -> list A -> list A.

(*|
.. coq:: no-in
|*)

Arguments Nil {A}.

(*|
A pretty cool detail: Coq provides great facilities to extend the grammar of the language.
(Great as in extremely expressive, but hence can get tricky to use!)
|*)

Notation "'[]'" := Nil.
Notation "hd '::' tl" := (Cons hd tl).

(*|
Here's a trivial recursive version of the [length] of a list
|*)

Fixpoint length {A} (l : list A) : nat :=
  match l with
  | [] => O
  | _ :: tl => S (length tl)
  end.

(*|
And here is a version using [fold]
|*)

Fixpoint fold {A B} (f : A -> B -> B) (acc : B) (l: list A) :=
  match l with
  | [] => acc
  | x :: xs => f x (fold f acc xs)
  end.

Definition length' {A} : list A -> nat := fold (fun _ k => S k) O.

(*|
Can you prove that they are (extensionally) equal?
|*)

Lemma length_is_length' :
  forall {A} (l : list A),
    length l = length' l.
Proof.
  intros A l.
  induction l.
  - reflexivity.
  - cbn.
    rewrite IHl.
    reflexivity.
Qed.

(*|
.. coq:: no-in
|*)

End PlayGround.

Module Logic.

(*|
Coq: expressing mathematical statements
=======================================

Prop
--------
We have defined so far several datatypes : nats, booleans, lists, ... Each of these were devised to intuitively represent collections of objects, each of which we care to distinguish.
As such, they have all had the sort [Type]. We have then defined functions computing specific elements of these types, and even reasoned a bit about the specific elements these functions compute.

I have actually lied slightly; there are a few cases we have gone off trail : when we have stated these properties.
Indeed, we have pondered whether two terms were equal: let's check the type of such a foe.
|*)
Check (3 = 2).

(*|
The answer we get is [Prop], the sort of propositions.
There are a few subtleties differentiating [Type] and [Prop], but for Today we will stick to a simple pragmatic idea:
- we define in [Type] the collections of which we care about the particular elements, and whose functions we care about the computational content: that is what you would define in traditional programming languages
- we define in [Prop] the properties, of which we only care if they are inhabited (i.e. they hold true) or if they are not inhabited (i.e. they are False).

Let's do a little bit of elementary logic to explore this.
|*)

(*|
What is our traditional notion of truth? It's the type that has a unique inhabitant:
|*)

Inductive True : Prop := I : True.

(*|
And of course what is False? The vaccuous type, that contains no inhabitant.
As an inductive type, it has simply no constructors: it is impossible to build a value for this type.
|*)

Inductive False : Prop :=.

(*|
Naturally, we can define the propositional combinators we are used to manipulate.
For instance the conjunction of two propositions: notice that the constructor is the usual introduction rule.
|*)

(*|
.. coq:: no-in
|*)

Module NotationConflict.

(*|
.. coq:: in
|*)

Inductive and (A B : Prop) : Prop := conj : A -> B -> and A B.

(*|
EXERCISE! Can you define the disjunction?
|*)

End NotationConflict.

(*|
Let's prove that [and] is associative!
|*)

Theorem and_assoc : forall P Q R : Prop,
  P /\ (Q /\ R) -> (P /\ Q) /\ R.
Proof.
  Show Proof.
  intros P Q R H.
  Show Proof.
  destruct H.
  Show Proof.
  destruct H0.
  Show Proof.
  split.
  Show Proof.
  - split.
    + exact H.
    + exact H0.
  - exact H1.
Qed.

(*|
EXERCISE! Can you write the proof without using any tactic, by programming it directly? You can simply print [and_assoc] to check for a possible answer.
|*)

(*|
Note: Coq does not assume excluded middle by default, but it is compatible with it: it is up to the user to decide whether they want to prove things constructively or not.
|*)

Definition excluded_middle := forall P : Prop,
  P \/ ~ P.

(*|
By using functions into [Prop], we can define properties of the arguments of this function.
For instance, here is the property of a function to be injective.
|*)

Definition injective {A B} (f : A -> B) :=
  forall x y : A, f x = f y -> x = y.

(*|
But since we do not care about the computational content, we even get an additional liberty: we can define such properties using inductive types.
Here is the property of a natural number to be even
|*)

Inductive even : nat -> Prop :=
| evenO  : even 0
| evenSS : forall n, even n -> even (S (S n))
.

(*|
The latter style has an attracting aspect: as our inductive data-types came with induction principles, so do our inductive properties!
Think for instance when you establish a property by induction on the type derivation of a term: you are not inducting over the term, nore the type, but over the proof of well-typedness of the term at a type.
|*)

Fixpoint double (n:nat) :=
  match n with
  | O => O
  | S n' => S (S (double n'))
  end.

Lemma even_double : forall n,
  even n -> exists (k : nat), n = double k.
Proof.
  intros.
  induction H.
  - exists 0. reflexivity.
  - destruct IHeven as (k & eq).
    exists (S k).
    subst.
    reflexivity.
Qed.

End Logic.

(*|
Coq: reasoning about programming languages
==========================================

Coq can be used to formalize most mathematics. But for us in particular, an interesting domain of application is the formalization of the semantics of programming languages.

.. coq:: no-in
|*)

From Coq Require Import Arith ZArith Strings.String Program.Equality.

Local Open Scope string_scope.
Local Open Scope Z_scope.
Local Open Scope list_scope.

Module AExp.

(*|
Pure expressions
-----------------

Let's start by defining the abstract syntax of a little language of arithmetic expressions.
|*)

Inductive aexp : Type :=
  | CONST (n: Z)                       (**r a constant, or *)
  | PLUS (a1: aexp) (a2: aexp)         (**r a sum of two expressions, or *)
  | MINUS (a1: aexp) (a2: aexp).       (**r a difference of two expressions *)

(*|
This mini-language is particularly simple: it is pure, in particular it can neither fail nor diverge. We can therefore easily represent its semantics directly as Gallina computations by implementing an interpreter.
|*)

Fixpoint aeval (a: aexp) : Z :=
  match a with
  | CONST n => n
  | PLUS a1 a2 => aeval a1 + aeval a2
  | MINUS a1 a2 => aeval a1 - aeval a2
  end.

(*|
Let's get ambitious and prove a quite complex optimization: we can substitute "0 + e" by "e" in arithmetic expressions!

First, we need to program our optimization as a Gallina function.
|*)

Fixpoint optimize_0plus (a:aexp) : aexp :=
  match a with
  | CONST n => CONST n
  | PLUS (CONST 0) e2 => optimize_0plus e2
  | PLUS e1 e2 => PLUS (optimize_0plus e1) (optimize_0plus e2)
  | MINUS e1 e2 => MINUS (optimize_0plus e1) (optimize_0plus e2)
  end.

(*|
Now we can state our theorem: any arithmetic expression has the same semantics before and after optimization.

Things are looking good for us: both the semantics and the optimization are defined by recursion on the structure of our expressions, we have a good chance of success by induction on this structure!
|*)

Theorem optimize_0plus_sound: forall a,
  aeval (optimize_0plus a) = aeval a.
Proof.
  intros a. induction a.
  - (* CONST *) reflexivity.
  - (* PLUS *) destruct a1 eqn:Ea1.
    + (* a1 = CONST n *) destruct n eqn:En.
      * (* n = 0 *)  simpl. apply IHa2.
      * (* n > 0 *) simpl. rewrite IHa2. reflexivity.
      * (* n < 0 *) simpl. rewrite IHa2. reflexivity.
    + (* a1 = PLUS a1_1 a1_2 *)
      simpl. simpl in IHa1. rewrite IHa1.
      rewrite IHa2. reflexivity.
    + (* a1 = MINUS a1_1 a1_2 *)
      simpl. simpl in IHa1. rewrite IHa1.
      rewrite IHa2. reflexivity.
  - (* MINUS *)
    simpl. rewrite IHa1. rewrite IHa2. reflexivity.
Qed.

(*|
Naturally, we don't have quite enough for a paper, this result is not too impressive. However it is already a good support to pause and think about what it means to prove such a result in Coq.

We have defined:
- a syntax
- a semantics
- an optimization
- a proof of soundness of the optimization.

In a way, what Coq provides us is the guarantee that all of these components are what they claim (in particular, that the proof _is_ a proof), but also that they _actually_ talk about one another.
There is no ambiguity about what language a result talks about, things are properly specified. This raises majors questions about maintainability and evolution of formal language: if add a construction,
or amend a semantic rule, then my proof will likely break. But it ensures that no bad interaction between two features that would have been checked sound separately on their own right could sneak in.

So what should you keep an eye for when wondering whether a claim of the flavor of "I've formalized my work in Coq" means much?
- If the object they have formalized is the same as the object they claim to talk about. This is very much a process of modelization, it can be as crude and non-sensical as in any other context!
- If the theorem they prove states something interesting. In particular if they don't constrain the scope of the result by adding assumptions.
- Marginally if they don't use axioms or admit results, but this is a simpler problem.

.. coq:: no-in
|*)

End AExp.

Module Imp.

(*|
Impure expressions
---------------------

Let's add some states!
|*)

Definition ident := string.

Inductive aexp : Type :=
  | CONST (n: Z)                       (**r a constant, or *)
  | VAR (x: ident)                     (**r a variable, or *)
  | PLUS (a1: aexp) (a2: aexp)         (**r a sum of two expressions, or *)
  | MINUS (a1: aexp) (a2: aexp).       (**r a difference of two expressions *)

(** The denotational semantics: an evaluation function that computes
  the integer value denoted by an expression.  This function is
  parameterized by a store [s] that associates values to variables. *)

Definition store : Type := ident -> Z.

Fixpoint aeval (a: aexp) (s: store) : Z :=
  match a with
  | CONST n => n
  | VAR x => s x
  | PLUS a1 a2 => aeval a1 s + aeval a2 s
  | MINUS a1 a2 => aeval a1 s - aeval a2 s
  end.

(** The IMP language has conditional statements (if/then/else) and
  loops.  They are controlled by expressions that evaluate to Boolean
  values.  Here is the abstract syntax of Boolean expressions. *)

Inductive bexp : Type :=
  | TRUE                              (**r always true *)
  | FALSE                             (**r always false *)
  | EQUAL (a1: aexp) (a2: aexp)       (**r whether [a1 = a2] *)
  | LESSEQUAL (a1: aexp) (a2: aexp)   (**r whether [a1 <= a2] *)
  | NOT (b1: bexp)                    (**r Boolean negation *)
  | AND (b1: bexp) (b2: bexp).        (**r Boolean conjunction *)

(** Just like arithmetic expressions evaluate to integers,
  Boolean expressions evaluate to Boolean values [true] or [false]. *)

Fixpoint beval (b: bexp) (s: store) : bool :=
  match b with
  | TRUE => true
  | FALSE => false
  | EQUAL a1 a2 => aeval a1 s =? aeval a2 s
  | LESSEQUAL a1 a2 => aeval a1 s <=? aeval a2 s
  | NOT b1 => negb (beval b1 s)
  | AND b1 b2 => beval b1 s && beval b2 s
  end.

(** There are many useful derived forms. *)

Definition NOTEQUAL (a1 a2: aexp) : bexp := NOT (EQUAL a1 a2).

Definition GREATEREQUAL (a1 a2: aexp) : bexp := LESSEQUAL a2 a1.

Definition GREATER (a1 a2: aexp) : bexp := NOT (LESSEQUAL a1 a2).

Definition LESS (a1 a2: aexp) : bexp := GREATER a2 a1.

Definition OR (b1 b2: bexp) : bexp := NOT (AND (NOT b1) (NOT b2)).

(*|
Alright, let's now get serious: let's consider a Turing-complete language!
|*)

Inductive com: Type :=
  | SKIP                                     (**r do nothing *)
  | ASSIGN (x: ident) (a: aexp)              (**r assignment: [v := a] *)
  | SEQ (c1: com) (c2: com)                  (**r sequence: [c1; c2] *)
  | IFTHENELSE (b: bexp) (c1: com) (c2: com) (**r conditional: [if b then c1 else c2] *)
  | WHILE (b: bexp) (c1: com).               (**r loop: [while b do c1 done] *)

(*|
And let's define a few fancy notations for convenience.
|*)

Bind Scope imp_scope with com.
Notation "'SKIP'" :=
   SKIP : imp_scope.
Notation "x '::=' a" :=
  (ASSIGN x a) (at level 60) : imp_scope.
Notation "c1 ;; c2" :=
  (SEQ c1 c2) (at level 80, right associativity) : imp_scope.
(* Notation "'WHILE' b 'DO' c 'END'" := *)
  (* (WHILE b c) (at level 80, right associativity) : imp_scope. *)
Notation "'TEST' c1 'THEN' c2 'ELSE' c3 'FI'" :=
  (IFTHENELSE c1 c2 c3) (at level 80, right associativity) : imp_scope.
Open Scope imp_scope.
Open Scope string_scope.

(** Here is an IMP program that performs Euclidean division by
  repeated subtraction.  At the end of the program, "q" contains
  the quotient of "a" by "b", and "r" contains the remainder.
  In pseudocode:
<<
       r := a; q := 0;
       while b <= r do r := r - b; q := q + 1 done
>>
  In abstract syntax:
*)

Definition Euclidean_division :=
  "r" ::= VAR "a" ;;
  "q" ::= CONST 0 ;;
  WHILE (LESSEQUAL (VAR "b") (VAR "r"))
    (ASSIGN "r" (MINUS (VAR "r") (VAR "b")) ;;
     ASSIGN "q" (PLUS (VAR "q") (CONST 1))).

(*|
We are of course tempted to jump in and define a new Fixpoint for the semantics of our commands.

We just need an update function on our states, and we shoudl be good
|*)

Definition update (x: ident) (v: Z) (s: store) : store :=
  fun y => if string_dec x y then v else s y.

Lemma update_same: forall x v s, (update x v s) x = v.
Proof.
  unfold update; intros. destruct (string_dec x x); congruence.
Qed.

Lemma update_other: forall x v s y, x <> y -> (update x v s) y = s y.
Proof.
  unfold update; intros. destruct (string_dec x y); congruence.
Qed.

Fixpoint ceval_fun_no_while (c : com) (st : store) : store :=
  match c with
    | SKIP =>
        st
    | x ::= a1 =>
        update x (aeval a1 st) st
    | c1 ;; c2 =>
        let st' := ceval_fun_no_while c1 st in
        ceval_fun_no_while c2 st'
    | TEST b THEN c1 ELSE c2 FI =>
        if beval b st
          then ceval_fun_no_while c1 st
          else ceval_fun_no_while c2 st
    | WHILE b c =>
        st  (* bogus *)
  end.

(*|
As long as we don't worry about the loop, things of course go smoothly...

What if we just give it a shot?
|*)

Fail Fixpoint ceval_fun_hopeful_while (c : com) (st : store)
                          : store :=
  match c with
    | SKIP =>
        st
    | x ::= a1 =>
        update x (aeval a1 st) st
    | c1 ;; c2 =>
        let st' := ceval_fun_hopeful_while c1 st in
        ceval_fun_hopeful_while c2 st'
    | TEST b THEN c1 ELSE c2 FI =>
        if beval b st
          then ceval_fun_hopeful_while c1 st
          else ceval_fun_hopeful_while c2 st
    | WHILE b c =>
        if beval b st
          then
            ceval_fun_hopeful_while (WHILE b c) st
          else st
  end.


(*|
This definition fails with the following message: Coq "Cannot guess decreasing argument of fix", and it seems to a concern.

Indeed while we are happy to live in this world where programming and proving conflates, it can only happen at the cost of programming in a constraint environment, where the logic corresponding to the type system is sound.
As it turns out, diverging programs are sources of inconsistencies. Imagine the following imaginary recursive function:

   [Fixpoint bad (u : unit) : P := bad u.]

OCaml will happily typecheck it parametrically in P. However if we read it under Curry Howard, we just found a proof for any proposition!!

Recursive functions in Coq are therefore severely restricted in Coq: they must all terminate. Coq has some structural criteria to prove it automatically by structural arguments, and provide facilities to manually provide a measure and prove the termination of our functions.

However here there is no hope: our interpreter indeed diverges for some inputs! We will hence switch gears and define a propositional semantics to Imp.

We will follow closely a style we can find in many semantic papers: a set of big-step reduction rules.
It's not the most pleasant to parse, btu it really is not more complex.

For instance, the rule of sequence is exactly saying:

if [c1,st ->* st'] and [c2,st' ->* st'']

then [c1;c2,st ->* st'']

.. coq:: in
|*)

Inductive red: com * store -> com * store -> Prop :=
  | red_assign: forall x a s,
      red (ASSIGN x a, s) (SKIP, update x (aeval a s) s)
  | red_seq_done: forall c s,
      red (SEQ SKIP c, s) (c, s)
  | red_seq_step: forall c1 c s1 c2 s2,
      red (c1, s1) (c2, s2) ->
      red (SEQ c1 c, s1) (SEQ c2 c, s2)
  | red_ifthenelse: forall b c1 c2 s,
      red (IFTHENELSE b c1 c2, s) ((if beval b s then c1 else c2), s)
  | red_while_done: forall b c s,
      beval b s = false ->
      red (WHILE b c, s) (SKIP, s)
  | red_while_loop: forall b c s,
      beval b s = true ->
      red (WHILE b c, s) (SEQ c (WHILE b c), s).

(*|
By moving from an interpreter to a propositional caracterisation of the semantics, we have gained a lot of freedom, at the cost of static guarantees.

We have not had to worry about any question of termination.
But note that we have actually simply modeled _exclusively_ the finite executions: this is captured by the inductive interpretation of our rules. Only finite trees can be
built, hence only finite computations are modeled.

But we actually got new perks. Is our semantics only partially defined? Easy, no one asked our rules to be total! (Actually divergence is precisely a source of partiality)
Is our semantics non-deterministic? Easy, no one asked our rules to be deterministic!

But is our semantics deterministic btw?

|*)

(*|
Indeed: an interesting property of this reduction relation is that it is
functional: a configuration [(c,s)] reduces to at most one configuration.
This corresponds to the fact that IMP is a deterministic language.
|*)

Lemma red_determ:
  forall cs cs1, red cs cs1 -> forall cs2, red cs cs2 -> cs1 = cs2.
Proof.
  induction 1; intros cs2 R2; inversion R2; subst; eauto.
- inversion H3.
- inversion H.
- assert (EQ: (c2, s2) = (c4, s3)) by auto. congruence.
- congruence.
- congruence.
Qed.

(*|
We define the semantics of a command by chaining successive reductions.
The command [c] in initial state [s] terminates with final state [s']
if we can go from [(c, s)] to [(skip, s')] in a finite number of reductions.
|*)

Inductive star {A : Type} (R : A -> A -> Prop): A -> A -> Prop :=
  | star_refl: forall a,
      star R a a
  | star_step: forall a b c,
      R a b -> star R b c -> star R a c.

Lemma star_one:
  forall {A : Type} (R : A -> A -> Prop) (a b: A), R a b -> star R a b.
Proof.
 intros. eapply star_step. eassumption. apply star_refl.
Qed.

Lemma star_trans:
  forall {A : Type} (R : A -> A -> Prop) (a b: A),
     star R a b -> forall c, star R b c -> star R a c.
Proof.
  intros A R a b STAR.
  induction STAR; intros ? STAR'.
  - assumption.
  - eapply star_step.
    eassumption.
    eauto.
Qed.

Definition terminates (s: store) (c: com) (s': store) : Prop :=
  star red (c, s) (SKIP, s').

(*|
Likewise, command [c] diverges (fails to terminate) in initial state [s]
    if there exists an infinite sequence of reductions starting with [(c, s)].
    The [infseq] relation operator is defined in library [Sequences].
|*)

Definition infseq {A : Type} (R : A -> A -> Prop) (a: A) : Prop :=
  exists X: A -> Prop,
  X a /\ (forall a1, X a1 -> exists a2, R a1 a2 /\ X a2).

Definition diverges (s: store) (c: com) : Prop :=
  infseq red (c, s).

(*|
  Last possible kind of execution: a buggy one.
  We can however prove that Imp admits no such executions.
|*)

Definition irred {A} (R : A -> A -> Prop) (a: A) : Prop := forall b, ~(R a b).

Definition goes_wrong (s: store) (c: com) : Prop :=
  exists c', exists s',
  star red (c, s) (c', s') /\ irred red (c', s') /\ c' <> SKIP.

Lemma red_progress:
  forall c s, c = SKIP \/ exists c', exists s', red (c, s) (c', s').
Proof.
  induction c; intros; auto; right.
  - do 2 eexists; constructor.
  - destruct (IHc1 s) as [? | (c' & s' & RED)].
    + subst. do 2 eexists; constructor; eauto.
    + do 2 eexists; constructor; eauto.
  - destruct (IHc1 s) as [? | (c' & s' & RED)].
    + subst. do 2 eexists; constructor; eauto.
    + do 2 eexists; constructor; eauto.
  - destruct (beval b s) eqn:EQ.
    + do 2 eexists; apply red_while_loop; eauto.
    + do 2 eexists; constructor; eauto.
Qed.

Lemma not_goes_wrong:
  forall c s, ~(goes_wrong s c).
Proof.
  intros c s (c' & s' & STAR & IRRED & NOTSKIP).
  pose proof (red_progress c' s') as [-> | (c'' & s'' & H)].
  easy.
  eapply IRRED; eauto.
Qed.












(** ** Big step semantics (sometimes called "natural semanics") *)

Inductive cexec: store -> com -> store -> Prop :=
  | cexec_skip: forall s,
      cexec s SKIP s
  | cexec_assign: forall s x a,
      cexec s (ASSIGN x a) (update x (aeval a s) s)
  | cexec_seq: forall c1 c2 s s' s'',
      cexec s c1 s' -> cexec s' c2 s'' ->
      cexec s (SEQ c1 c2) s''
  | cexec_ifthenelse: forall b c1 c2 s s',
      cexec s (if beval b s then c1 else c2) s' ->
      cexec s (IFTHENELSE b c1 c2) s'
  | cexec_while_done: forall b c s,
      beval b s = false ->
      cexec s (WHILE b c) s
  | cexec_while_loop: forall b c s s' s'',
      beval b s = true -> cexec s c s' -> cexec s' (WHILE b c) s'' ->
      cexec s (WHILE b c) s''.

(** The predicate [cexec s c s'] holds iff there exists a finite derivation
    for this conclusion, using the axioms and inference rules above.

    The structure of this derivation represents the computations performed
    by [c] as a tree --- not as a sequence of reductions.

    The finiteness of the derivation guarantees that only terminating executions
    satisfy [cexec].  As an example, [WHILE TRUE SKIP] does not satisfy [cexec].
*)

Lemma cexec_infinite_loop:
  forall s, ~ exists s', cexec s (WHILE TRUE SKIP) s'.
Proof.
  assert (A: forall s c s', cexec s c s' -> c = WHILE TRUE SKIP -> False).
  { induction 1; intros EQ; inversion EQ.
  - subst b c. cbn in H. discriminate.
  - subst b c. apply IHcexec2. auto.
  }
  intros s (s' & EXEC). apply A with (s := s) (c := WHILE TRUE SKIP) (s' := s'); auto.
Qed.










(*|
Of course we can prove properties of specific programs.

In particular, we can prove their functional correctness. Behold a colossal example.
|*)

Definition plus2 : com :=
  "X" ::= PLUS (VAR "X") (CONST 2).

Theorem plus2_spec : forall (st : store) (n : Z) (st' : store),
  st "X" = n ->
  cexec st plus2 st' ->
  st' "X" = n + 2.
Proof.
  intros st n st' HX Heval.
  inversion Heval. subst. clear Heval.
  simpl.
  apply update_same.
Qed.

(*|
Or in a different style, we can prove that the following infinite loop
indeed diverges (more specifically, we will state that it admits no finite derivation. It is strictly weaker, as the rules could simply be partial).
|*)

Definition loop : com :=
  WHILE TRUE SKIP.

Theorem loop_never_stops : forall st st',
  ~(cexec st loop st').
Proof.
  intros st st' contra. unfold loop in contra.
  remember (WHILE TRUE SKIP) as loopdef
           eqn:Heqloopdef.
  induction contra; try now inversion Heqloopdef.
  inversion Heqloopdef; subst; clear Heqloopdef.
  simpl in H.
  inversion H.
Qed.















(*|
  However in this course we will be interested not in proving correct
  Imp programs themselves, but rather in proving correct meta-programs
  manipulating Imp programs: analyses, optimizations, compilers as a
  whole.
|*)

Definition correct_transformation_naive (T : com -> com) :=
  forall s c s',
    cexec s c s' ->
    cexec s (T c) s'.















(* Termination-sensitive? *)
Goal forall s s',
      cexec s (WHILE TRUE SKIP) s' ->
      cexec s SKIP s'.
intros s s' SEM.
exfalso.
eapply loop_never_stops.
apply SEM.
Qed.
(* Note: what if our language was not deterministic? *)












(*|
 We now show an equivalence between evaluations that terminate according
    to the natural semantics
        (existence of a derivation of [cexec s c s'])
    and to the reduction semantics
        (existence of a reduction sequence from [(c,s)] to [(SKIP, s')].

    We start with the natural semantics => reduction sequence direction,
    which is proved by an elegant induction on the derivation of [cexec].
|*)

(*|
A sequence of reductions can take place to the left
    of a [SEQ] constructor.  This generalizes rule [red_seq_step].
|*)

Lemma red_seq_steps:
  forall c2 s c s' c',
  star red (c, s) (c', s') -> star red ((c;;c2), s) ((c';;c2), s').
Proof.
  intros * STAR.
  dependent induction STAR.
- apply star_refl.
- destruct b as [c1 st1].
  apply star_step with (c1;;c2, st1). apply red_seq_step. auto. auto.
Qed.

Theorem cexec_to_reds:
  forall s c s', cexec s c s' -> star red (c, s) (SKIP, s').
Proof.
  intros s c s' RED. induction RED.
- (* SKIP *)
  apply star_refl.
- (* ASSIGN *)
  apply star_one. apply red_assign.
- (* SEQ *)
  eapply star_trans. apply red_seq_steps. apply IHRED1.
  eapply star_step.  apply red_seq_done.  apply IHRED2.
- (* IFTHENELSE *)
  eapply star_step. apply red_ifthenelse. auto.
- (* WHILE stop *)
  apply star_one. apply red_while_done. auto.
- (* WHILE loop *)
  eapply star_step. apply red_while_loop. auto.
  eapply star_trans. apply red_seq_steps. apply IHRED1.
  eapply star_step. apply red_seq_done. apply IHRED2.
Qed.

(** The other direction, from a reduction sequence to a [cexec]
    derivation, is more subtle.  Here is the key lemma, showing how a
    reduction step followed by a [cexec] execution can combine into a
    [cexec] execution. *)

Lemma red_append_cexec:
  forall c1 s1 c2 s2, red (c1, s1) (c2, s2) ->
  forall s', cexec s2 c2 s' -> cexec s1 c1 s'.
Proof.
  intros * STEP.
  dependent induction STEP; intros.
- (* red_assign *)
  inversion H; subst. apply cexec_assign.
- (* red_seq_done *)
  apply cexec_seq with s2. apply cexec_skip. auto.
- (* red seq step *)
  inversion H; subst. apply cexec_seq with s'0.
  eapply IHSTEP; eauto.
  auto.
- (* red_ifthenelse *)
  apply cexec_ifthenelse. auto.
- (* red_while_done *)
  inversion H0; subst. apply cexec_while_done. auto.
- (* red while loop *)
  inversion H0; subst. apply cexec_while_loop with s'0; auto.
Qed.

(** By induction on the reduction sequence, it follows that a command
    that reduces to [SKIP] executes according to the natural semantics,
    with the same final state. *)

Theorem reds_to_cexec:
  forall s c s',
  star red (c, s) (SKIP, s') -> cexec s c s'.
Proof.
  intros * STAR. dependent induction STAR.
- apply cexec_skip.
- destruct b as [c1 s1]. apply red_append_cexec with c1 s1; auto.
Qed.

(*|
.. coq:: no-in
|*)

End Imp.
